#!/bin/sh
mom_compile()
{
	refer $REFER_C_ARGS "$FILE" \
		| groff -mom $GROFF_C_ARGS > "$BIN.pdf"
}

exec mom_compile "$@"
