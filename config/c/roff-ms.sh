#!/bin/sh
ms_compile()
{
	refer $REFER_C_ARGS "$FILE" \
		| groff -me -ms $GROFF_C_ARGS > "$BIN.pdf"
}

exec ms_compile "$@"
