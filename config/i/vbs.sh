#!/bin/sh
# GUI VBScript on Wine
#exec wine wscript "$FILE" "$@" 2>/dev/null

# CLI VBScript on Wine
exec wine cscript "$FILE" "$@" 2>/dev/null
