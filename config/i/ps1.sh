#!/bin/sh
# Powershell Core for Unix
#exec pwsh "$FILE" "$@"

# Powershell on Wine
exec wine pwsh "$FILE" "$@" 2>/dev/null
