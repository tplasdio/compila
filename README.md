<p align="center">
  <h1 align="center">Compila</h1>
  <p align="center">
    Compile or interpret many programming languages with one command
  </p>
</p>

## 🚀 Installation

<details>
<summary>📦 From source</summary>

- *Dependencies*:
  - `sh`
  - [`getoptions`](https://github.com/ko1nksm/getoptions)
- *Optional Dependencies*:
  - The respective compilers/interpreters for each language. For example: `gcc`, `python`, `rustc`, `luajit`, etc.

```sh
git clone --filter=blob:none https://codeberg.org/tplasdio/compila.git
cd compila
INSTALL_CONFIGS=1 ./make.sh install  # or
# make install INSTALL_CONFIGS=1
```

Run the `make.sh` as root for system-installation.

For uninstallation, run either `./make.sh uninstall` or `make uninstall`.

</details>

## ⭐ Usage

```sh
compila file.c             # Compile to binary
compila file.py            # Compile to bytecode
compila -r file.c          # Compile and run
compila -i file.py         # Interpret
compila -ri file.py        # Interpret or compile and run
compila -t c file.cpp      # Compile with a certain file type
compila -v file.c          # Show command being run
compila -v file.c -- -O2   # Pass other arguments to the compiler/interpreter
compila -help              # Show help
```

## ⚙ Configuration

This program basically just runs a source file with a given configuration
script in `$config/c/<filetype>.sh` for compilers or `$config/i/<filetype>.sh`
for interpreters. Also you can pass variables to those scripts from the
`$config/compilarc` file.

For example to configure C compilation, you edit `$config/c/c.sh`:

```sh
#!/bin/sh
exec gcc "$FILE" -o "$BIN" $C_C_ARGS "$@"  # ← you may change gcc for clang compiler
```

or the arguments in `$config/compilarc`:
```sh
export C_C_ARGS='-Ofast -std=c2x'   # ← arguments passed to the compiler
```

The location of the $config directory is `$XDG_CONFIG_HOME/compila` or
`~/.config/compila` by default.

Note that for interpreters, if a shebang (e.g. `#!/bin/sh`) is found
in the source file, then the file is given executable permission and
run with said shebang instead of the configuration file.

## 💭 Similar
- Neovim's `makeprg` compilers: `nvim --headless -c 'make|q'`
- [scriptisto](https://github.com/igor-petruk/scriptisto)
- [Compiler.nvim](https://github.com/Zeioth/Compiler.nvim)
- [sniprun](https://github.com/michaelb/sniprun)
- [Luke Smith's compiler](https://github.com/LukeSmithxyz/voidrice/blob/master/.local/bin/compiler)

Managing multiple compilers or interpreters is a common capability of
many IDEs or text editors, but I haven't found any standalone tool
that decouples this and lets you run a simple command from the terminal,
hence why I made this.

## 📝 License
GPLv3 or later
