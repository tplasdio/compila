#!/bin/sh

setup() {

	: \
	"${DESTDIR:="/"}" \
	"${PRG:=compila}"

	PRG_PATH=compila \
	CONFIG_PATH=config \
	LICENSE="LICENSE" \
	XDG_PREFIX="${HOME}/.local"

	[ "$(id -u)" -eq 0 ] || [ "$FORCEROOT" = 1 ] \
		&& isroot=: \
		|| isroot=false

	$isroot \
		&& prefix="${PREFIX:=/usr/local}" \
		|| prefix="${XDG_PREFIX}"

	bindir="$prefix/bin" \
	licensedir="$prefix/share/licenses/$PRG" \
	configdir="${XDG_CONFIG_HOME:-"$HOME/.config"}/$PRG"
}

install()
{
	mkdir -p -- "${DESTDIR}/${bindir}" "${DESTDIR}/${licensedir}" "${configdir}"
	chmod 755 -- "${PRG_PATH}"
	cp -- "${PRG_PATH}" "${DESTDIR}/${bindir}/${PRG}"
	if [ "${INSTALL_CONFIGS}" ]; then
		cp -R -- "${CONFIG_PATH}"/* "${DESTDIR}/${configdir}"
	fi
	cp -- "$LICENSE" "${DESTDIR}/${licensedir}/LICENSE"
}

uninstall()
{
	rm -f -- "${DESTDIR}/${bindir}/${PRG}"
	rm -rf -- "${DESTDIR}/${licensedir}"
}

main()
{
	set -x
	set -e
	setup || return $?
	target="${1-install}"

	case "$target" in
	(i|install)   install;;
	(u|uninstall) uninstall;;
	(*)
		>&2 printf -- 'Unknown target given: "%s"\n' "$1"
		return 22
	esac
}

main "$@" || exit $?
