#!/usr/bin/make -f

VARS = \
	PRG='$(PRG)' \
	DESTDIR='$(DESTDIR)' \
	PREFIX='$(PREFIX)' \
	FORCEROOT='$(FORCEROOT)' \
	INSTALL_CONFIGS='$(INSTALL_CONFIGS)'

install:
	$(VARS) ./make.sh install

uninstall:
	$(VARS) ./make.sh uninstall
